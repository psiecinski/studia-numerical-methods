import math
import numpy as np
from scipy.misc import derivative as d
import matplotlib.pyplot as plt


def derivative(f, a, method, h):
    if method == 1:
        return (f(a) - f(a - h)) / h
    elif method == 2:
        return (f(a + h) - f(a - h))/(2*h)


print(derivative(math.cos, 0.3, 1, 0.01))
print(derivative(math.cos, 0.3, 2, 0.01))

x = np.linspace(0, 5*np.pi, 100)
y = derivative(np.cos, x, 1, 0.001) #podpunkt a
# y = derivative(np.cos, x, 2, 0.001) #podpunkt b


# wykres funkcji cos(x) oraz -sin(x)
plt.figure(figsize=(12, 5))
plt.plot(x, y, 'r.', label='Approx. derivative value')
plt.plot(x, -np.sin(x), 'b', label='Exact value')
plt.title("Derivative od cnvmos(x)")
plt.legend(loc='best')
plt.show()

# #jak zmienia sie blad
htab = np.logspace(-15, -1, 150)
y = [np.absolute(derivative(np.cos, 0.3, 1, num) - (-np.sin(0.3))) for num in htab] #podpunkt a
# y = [np.absolute(derivative(np.cos, 0.3, 2, num) - (-np.sin(0.3))) for num in htab] #podpunkt b

plt.plot(htab, y, "b.")
plt.xscale("log")
plt.yscale("log")
plt.xlabel("h")
plt.ylabel("błąd")
plt.show()
