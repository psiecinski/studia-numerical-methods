import numpy as np
from decimal import Decimal
import copy

def createBands(N): #4 wstegi
    w1 = []
    w2 = []
    w3 = []
    w4 = []
    for i in range(0,N):
        w1.append(1.2)
        if(i>0):
            w2.append(0.1/i)
            w3.append(0.4/pow(i,2))
        w4.append(0.2)
    return w1,w2,w3,w4

def createVectorB(n):
    array = []
    for i in range(1,n+1):
        array.append(i)
    return array

def lu_decomposition(N, w1, w2, w3, w4):
    for element in range(1,N):
        if element < N-1:
            w4[element-1] = w4[element-1]/w1[element-1]
            w1[element] = w1[element]-w4[element-1]*w2[element-1]
            w2[element] = w2[element]-w4[element-1]*w3[element-1] 
        else:
            w4[element-1] = w4[element-1]/w1[element-1]
            w1[element] = w1[element]-w4[element-1]*w2[element-1]
    return w1, w2, w3, w4

def backwardSubstitution(N, w1, w2, w3, z):
    i = 0
    wynik = []
    for element in range(N-1,0,-1):
        if element == N-1:
            wynik.append(z[element]/w1[element])
        elif element == N-2:
            wynik.append((z[element]-(w2[element]*wynik[i]))/w1[element])
            i += 1
        else:
            wynik.append((z[element]-(w2[element]*wynik[i])-w3[element]*wynik[i-1])/w1[element])
            i += 1
    return wynik

def count(N, w4, vectorB):
    arr = [1]
    i = 0
    for i in range(1, N):
        if i == 1:
            arr.append(vectorB[i])
            arr[i] = arr[i]-w4[i-1]
        else:
            arr.append(vectorB[i])
            arr[i] = arr[i]-w4[i-1]*arr[i-1]
    return arr

def determinant(A):
    temp = 1
    for i in A:
        temp = temp * i
    return temp

N = 100
w1, w2, w3, w4 = createBands(N)
w1, w2, w3, w4 = lu_decomposition(N,w1,w2,w3,w4)
vectorB = createVectorB(N)
z = count(N, w4, vectorB)
wynik = backwardSubstitution(N, w1, w2, w3, z)
detMatrix = determinant(w1)

print(wynik[::-1])
print("Wyznacznik macierzy: ", '%.2E' % Decimal(detMatrix))
