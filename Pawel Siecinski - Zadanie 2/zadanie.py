import math
import numpy as num
from decimal import Decimal

delty = []
wektorb = num.array([5.40780228, 3.67008677, 3.12306266, -1.11187948, 0.54437218])
wektorbprim = wektorb + num.array([pow(10, -5), 0, 0, 0, 0])

macierzA1 = num.array([[2.40827208, -0.36066254, 0.80575445, 0.46309511, 1.20708553],
                        [-0.36066254, 1.14839502, 0.02576113, 0.02672584, -1.03949556],
                        [0.80575445, 0.02576113, 2.45964907, 0.13824088, 0.0472749],
                        [0.46309511, 0.02672584, 0.13824088, 2.05614464, -0.9434493],
                        [1.20708553, -1.03949556, 0.0472749, -0.9434493, 1.92753926]])

macierzA2 = num.array([[2.61370745, -0.6334453, 0.76061329, 0.24938964, 0.82783473],
                [-0.6334453, 1.51060349, 0.08570081, 0.31048984, -0.53591589],
                [0.76061329, 0.08570081, 2.46956812, 0.18519926, 0.13060923],
                [0.24938964, 0.31048984, 0.18519926, 2.27845311, -0.54893124],
                [0.82783473, -0.53591589, 0.13060923, -0.54893124, 2.6276678]])                        


def rozwiaz(macierz):
    wspolczynnik = max(abs(num.linalg.eigvals(macierz)))/min(abs(num.linalg.eigvals(macierz)))
    y = num.linalg.solve(macierz, wektorb)
    yprim = num.linalg.solve(macierz, wektorbprim) 
    delta = num.linalg.norm(abs(y-yprim))
    delty.append(delta)
    print("y = ", y)
    print("y' (prim) = ", yprim)
    print("wspolczynnik wynosi:", '%.2E' % Decimal(wspolczynnik))
    print("delta wynosi: ", '%.2E' % Decimal(delta))


print("========")
print("Macierz A1")
rozwiaz(macierzA1)
print("========")
print(" ")

print("========")
print("Macierz A2")
rozwiaz(macierzA2)
print("========")
print(" ")

print("Roznica delt wynosi: ")
print('%.2E' % Decimal(round(abs(delty[0] - delty[1]),7)))