import numpy as np
from decimal import Decimal

N = 50

def createBands(N):
    w1 = []
    w2 = []
    for i in range(N):
        w1.append(9)
        w2.append(7)
    return w1, w2

def createVectorB(N):
    return [5 for i in range(N)]

def createOnes(N):
    return [1 for i in range(N)]

def backwardSubstitution(N, w1, w2, vectorB, v):
    i = 0
    z = []
    q = []
    for element in range(N-1,-1,-1):
        if element==(N-1):
            z.append(vectorB[element]/w1[element])
            q.append(v[element]/w1[element])
        else:
            z.append((vectorB[element]-w2[element]*z[i])/w1[element])
            q.append((v[element]-w2[element]*q[i])/w1[element])
            i+=1
    return z, q


w1, w2 = createBands(N)
vectorB = createVectorB(N) # wektor b
v = createOnes(N) #same jedynki
z, q = backwardSubstitution(N, w1, w2, vectorB, v)


#==================
w = np.subtract(z, np.dot((np.dot(v, z)/(np.add(1, np.dot(v,q)))), q)) #wzor: z – ((v^T*z)/(1 + v^T*q))*q

print("w = ", w[::-1])
