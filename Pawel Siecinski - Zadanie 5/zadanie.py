import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from scipy.linalg import solve

N = 100

def jacobi(A, b, x0, N, TOL):
    if x0 is None:
        x0 = np.zeros(len(A[0]))
    n = len(A)
    k = 1
    copy = []
    while (k <= N):
        x = np.zeros(n)
        for i in range(n):
            for j in range(i):
                x[i] -= A[i,j]*x0[j]
            for j in range(i+1,n):
                x[i] -= A[i,j]*x0[j]
            x[i] = (x[i] + b[i])/A[i,i]
        if (np.linalg.norm(x-x0) < TOL):
            return x,k
        k += 1;
        x0 = np.copy(x)
        copy.append(x0[0])
    return x0, k, copy

def gaussseidel(A,b,x0, N,TOL):
    if x0 is None:
        x0 = np.zeros(len(A[0]))
    n = len(A)
    k = 1
    copys = []
    while (k <= N):
        x = np.zeros(n)
        for i in range(n):
            s = 0.0;
            for j in range(i):
                s += A[i,j]*x[j]
            for j in range(i+1,n):
                s += A[i,j]*x0[j]
            x[i] = (b[i] - s)/A[i,i]
        if (np.linalg.norm(x-x0) < TOL):
            return x,k,copys
        k += 1;
        x0 = np.copy(x)
        copys.append(x0[0])
    return x0, k, copys


def exactSolve(A, b, x=None):
    if x is None:
        x = np.zeros(len(A[0]))
    x = solve(A,b)
    np.dot(A,x) - b
    return x

def createMatrix(n):
    A = np.zeros(shape=(n,n))
    i,k = np.indices((n,n))
    A[i==k+1] = 1
    A[i==k] = 3
    A[i==k-1] = 1
    A[i==k-2] = 0.2
    A[i==k+2] = 0.2
    return A

def createB(N):
    return [x + 1 for x in range(N)]

matrix = createMatrix(N)
vectorB = createB(N)
solvedJacob = jacobi(matrix,vectorB, None , N, 0.00000000000001)
solvedSeidel = gaussseidel(matrix, vectorB, None , N, 0.00000000000001)
iterationsJacob = solvedJacob[1]
iterationsSeidel = solvedSeidel[1]
jacobiCopy = solvedJacob[2];
seidelCopy = solvedSeidel[2];
solvedExact = exactSolve(matrix, vectorB)

seidelError = abs(np.subtract(seidelCopy, solvedExact[0]))
jacobiError = abs(np.subtract(jacobiCopy, solvedExact[0]))

print("Liczba iteracji metoda Seidel: ", iterationsSeidel)
print("Liczba iteracji metoda Jacoba: ", iterationsJacob)
print("Dokladne rozwiazanie: \n", solvedExact)
print("Blad pomiaru przy uzyciu metody Seidla (wartosc 0): \n", seidelError)
print("Blad pomiaru przy uzyciu metody Jacoba (wartosc 0): \n", jacobiError)
plt.plot(seidelError, label = "Diff Seidel")
plt.plot(jacobiError, label = "Diff Jacob")
plt.yscale("log")
plt.ylabel('Error value')
plt.xlabel('Number of iterations')
plt.legend(loc="upper left")
plt.margins(0)
plt.step(0,100)
plt.show()

